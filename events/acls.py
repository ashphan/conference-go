from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_location_photo(city, state):
    try:
        url = "https://api.pexels.com/v1/search"
        params = {
            "query": city + " " + state,
            "per_page": 1,
        }
        headers = {"Authorization": PEXELS_API_KEY}

        response = requests.get(url, params=params, headers=headers)
        photo = json.loads(response.content)
        url = photo["photos"][0]["url"]
        return {"picture_url": url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
